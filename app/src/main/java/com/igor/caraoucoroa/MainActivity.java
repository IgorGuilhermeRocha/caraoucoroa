package com.igor.caraoucoroa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button btJogar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUIAttrs();
        setBtEvents();


    }

    private void initUIAttrs(){
        btJogar = findViewById(R.id.bt_jogar);
    }

    private void setBtEvents(){
        btJogar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        jogar();
                    }
                }
        );
    }


    public void jogar(){
        Random random = new Random();
        int caraOuCoroa = random.nextInt(2);
        Intent intent = new Intent(this, ResultadoActivity.class);
        intent.putExtra("caraOuCoroa", caraOuCoroa);
        startActivity(intent);
    }
}