package com.igor.caraoucoroa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ResultadoActivity extends AppCompatActivity {

    private Button btVoltar;
    private ImageView imgMoeda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        initUIAttrs();
        setBtEvents();
        Bundle bundle = getIntent().getExtras();
        mostraMoeda(bundle.getInt("caraOuCoroa"));

    }

    private void initUIAttrs(){
        btVoltar = findViewById(R.id.bt_voltar);
        imgMoeda = findViewById(R.id.img_moeda);
    }

    private void setBtEvents(){
        btVoltar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        voltar();
                    }
                }
        );
    }

    private void mostraMoeda(int caraOuCoroa){
        if(caraOuCoroa == 0) imgMoeda.setImageResource(R.drawable.moeda_coroa);
        else imgMoeda.setImageResource(R.drawable.moeda_cara);
    }
    public void voltar(){
        startActivity(new Intent(this, MainActivity.class));
    }
}